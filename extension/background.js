/* Listen for tab change and get its URL*/
/* Dispatch mutations from background to listen to popup */

let params = {
    active: true,
    currentWindow : true
}

function getTabUrl(tabs){
    console.log(tabs)
    chrome.tabs.onCreated.addListener(function(tab){
        console.log(tab)
    })
    
    chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
        chrome.storage.local.set({'currentUrl': changeInfo.url},function(){
            console.log(changeInfo.url)
        })
    })

    chrome.tabs.onActivated.addListener(function(tabId , changeInfo , tab){
        chrome.storage.local.set({'currentUrl': changeInfo.url}, function(){
            console.log('>>>>>', changeInfo.url)
        })
    })
}

chrome.tabs.query(params,getTabUrl);
