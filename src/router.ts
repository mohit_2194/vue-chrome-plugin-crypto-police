import Vue from 'vue';
import Router from 'vue-router';
import Home from './components/Home.vue';
import Websites from './components/Websites.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/websites',
      name: 'websites',
      component: Websites,
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './components/About.vue'),
    },
  ],
});
