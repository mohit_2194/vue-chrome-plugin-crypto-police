import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    websiteItems: []
  },

  getters: {
    websiteItems: state => state.websiteItems,
  },
  mutations: {
    saveWebsiteItems(state,data){
      state.websiteItems = data.websiteItems;
    },


  },
  actions: {

  },
});

